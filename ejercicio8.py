#Ejercicio de listas   NÚMERO 7.8 :
#Dada una lista de enteros, encuentre el primer elemento máximo en él.
#Imprime su valor y su índice (contando con 0)."""
#NOMBRE:VICTOR GAUMAN
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
maximo = max(a)
print(maximo, a.index(maximo))