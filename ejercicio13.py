#Ejercicio de listas   NÚMERO 8.4 2D :
#Dado un número entero n , cree una matriz bidimensional de tamaño n × n de acuerdo
#con las siguientes reglas e imprímalo:
#En el antidiagonal poner 1.
#En las diagonales arriba pone 0.
#En las diagonales debajo pone 2."""
#AUTOR:VICTOR GAUMAN

# Read an integer:
# a = int(input())
# Print a value:
# print(a)
n = int(input())
matriz = [[0] * n for i in range(n)]
for i in range(n):
  for j in range(n):
    if i + j + 1 < n:
      matriz[i][j] = 0
    elif i + j + 1 == n:
      matriz[i][j] = 1
    else:
      matriz[i][j] = 2
for fil_col in matriz:
  print(*fil_col)