#Ejercicio de listas   NÚMERO 7.4 :
#Dada una lista de enteros distintos de cero, encuentre e imprima el primer par
#de elementos adyacentes que tengan el mismo signo. Si no existe ese par, imprima 0"""
# Read a list of integers:
#AUTOR:VICTOR GUAMAN
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]

for num in range(1, len(a)):
  if a[num - 1] * a[num] > 0:
    print(a[num - 1], a[num])
    break
else:
  print(0)