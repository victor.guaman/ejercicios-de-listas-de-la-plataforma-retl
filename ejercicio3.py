#Ejercicio de listas  7.3.
#Dada una lista de números, encuentre e imprima todos sus elementos que sean
#mayores que su vecino izquierdo.
#AUTOR:VICTOR GUAMAN.
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
for num in range(1, len(a)):
    if a[num] > a[num-1]:
        print(a[num], end=' ')
