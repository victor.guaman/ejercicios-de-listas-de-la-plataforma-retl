#Ejercicio de listas   NÚMERO 8.5 2D :
#Dados dos enteros (el número de filas m y columnas n de m × n 2d lista) y
#las siguientes m filas de n enteros, seguidas por dos enteros no negativos
#i y j menores que n , intercambie las columnas i y j de la lista 2d e
#imprime el resultado."""
#AUTOR:VICTOR GAUMAN



# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
m, n = [int(s) for s in input().split()]
matriz = [[int(j) for j in input().split()] for i in range(m)]
col1, col2 = [int(s) for s in input().split()]
for i in range(m):
  matriz[i][col1], matriz[i][col2] = matriz[i][col2], matriz[i][col1]
for fil_col in matriz:
  print(*fil_col)