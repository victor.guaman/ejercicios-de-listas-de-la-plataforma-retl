#Ejercicio de listas  7.7 :
#Dada una lista de números, intercambie elementos adyacentes en cada par
#(intercambie A [0] con A [1] , A [2] con A [3] , etc.). Imprime la lista
#resultante. Si una lista tiene un número impar de elementos, deje el
#último elemento intacto."""
#AUTOR:VICTOR GUAMAN.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
x = input()
lista = list(map(int, x.split()))

for i in range(0, len(lista), 2):
  popval = lista.pop(i)
  lista.insert(i+1, popval)
  
print(lista)