#Ejercicio de listas   NÚMERO 7.D:

#En los bolos, el jugador comienza con 10 pines seguidos en el extremo más
#alejado de un carril. El objetivo es derribar todos los pines. Para esta
#tarea, el número de alfileres y bolas variará. Dado el número de pines N y
#luego el número de bolas K a rodar, seguido de K pares de números (uno para
#cada bola rodada), determine qué pines permanecen en pie después de que se hayan
#rodado todas las bolas.
#AUTOR:VICTOR GUAMAN



# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
n, k = [int(s) for s in input().split()]
pines = ['I'] * n
for i in range(k):
  izq, derecha = [int(s) for s in input().split()]
  for j in range(izq - 1, derecha):
    pines[j] = '.'
print(''.join(pines))