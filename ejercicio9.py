#Ejercicio de listas   NÚMERO 7.9 :
#Dada una lista de números distintos, intercambie el mínimo y el máximo e
#imprima la lista resultante."""
#AUTOR:VICTOR GUAMAN
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]

max, min = a.index(max(a)), a.index(min(a))
a[max], a[min] = a[min], a[max]

print(a)