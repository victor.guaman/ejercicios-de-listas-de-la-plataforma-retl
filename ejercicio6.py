#Ejercicio de listas   NÚMERO 7.6 :
#Dada una lista de números con todos los elementos ordenados en orden ascendente,
#determine e imprima el número de elementos distintos que contiene."""
#AUTOR:VICTOR GUAMAN
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]

total = 1
for i in range(1, len(a)):
  if (a[i-1] != a[i]):
    total += 1
print(total)
