#Ejercicio de listas   NÚMERO 7.C :
#Es posible colocar 8 reinas en un tablero de ajedrez de 8 × 8 para que no haya
#dos reinas que se amenacen entre sí. Por lo tanto, requiere que no haya dos reinas
#que compartan la misma fila, columna o diagonal.
#Dada una colocación de 8 reinas en el tablero de ajedrez. Si hay un par de reinas
#que viola esta regla, imprima SÍ , de lo contrario imprima NO . La entrada consta
#de ocho pares de coordenadas, un par por línea, y cada par da la posición de una
#reina en un tablero de ajedrez estándar con filas y columnas numeradas del 1 al 8"""
#AUTOR:VICTOR GUAMAN
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
x = []
y = []
for i in range(8):
  a = [int(s) for s in input().split()]
  x.append(a[0])
  y.append(a[1])
answer = 'NO'
for i in range(8):
  for j in range(i + 1, 8):
    if ((x[i] == x[j]) or
        (y[i] == y[j]) or
        (abs(x[i] - x[j]) == abs(y[i] - y[j]))):
      answer = 'YES'
print(answer)